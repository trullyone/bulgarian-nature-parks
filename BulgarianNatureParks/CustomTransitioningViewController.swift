//
//  CustomTransitioningViewController.swift
//  BulgarianNatureParks
//
//  Created by Nickolay Kunev on 02.10.14.
//  Copyright (c) 2014 г. Divine Bulgaria. All rights reserved.
//

import UIKit

public class CustomTransitioningViewController: UIViewController, UINavigationControllerDelegate {

    public var animatedTransitioningController : CustomBlockAnimatedTransitioning? = nil
    public var interactiveTransitioningController : CustomPercentDrivenInteractiveTransition? = nil
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    public override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!) {
        var dest = segue.destinationViewController as CustomTransitioningViewController
        dest.animatedTransitioningController = self.animatedTransitioningController
        dest.interactiveTransitioningController = self.interactiveTransitioningController
    }
}
