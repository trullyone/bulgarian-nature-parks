//
//  CustomTransitioningNavigationViewController.swift
//  BulgarianNatureParks
//
//  Created by Nickolay Kunev on 02.10.14.
//  Copyright (c) 2014 г. Divine Bulgaria. All rights reserved.
//

import UIKit

class CustomTransitioningNavigationViewController: UINavigationController, UINavigationControllerDelegate {

    var animatedTransitioningController : CustomBlockAnimatedTransitioning? = nil
    var interactiveTransitioningController : CustomPercentDrivenInteractiveTransition? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.interactiveTransitioningController = CustomPercentDrivenInteractiveTransition()
        self.animatedTransitioningController = CustomBlockAnimatedTransitioning()
        self.delegate = self
        
        var dest = self.topViewController as CustomTransitioningViewController
        dest.animatedTransitioningController = self.animatedTransitioningController
        dest.interactiveTransitioningController = self.interactiveTransitioningController
    }

    func navigationController(navigationController: UINavigationController, interactionControllerForAnimationController animationController: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning?
    {
        if(interactiveTransitioningController!.isInteractive){
            return interactiveTransitioningController
        }
        return nil
    }
    
    func navigationController(navigationController: UINavigationController, animationControllerForOperation operation: UINavigationControllerOperation, fromViewController fromVC: UIViewController, toViewController toVC: UIViewController) -> UIViewControllerAnimatedTransitioning?
    {
        return self.animatedTransitioningController
    }
}
