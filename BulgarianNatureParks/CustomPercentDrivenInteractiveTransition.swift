//
//  CustomBlockPercentDrivenInteractiveTransition.swift
//  BulgarianNatureParks
//
//  Created by Nickolay Kunev on 02.10.14.
//  Copyright (c) 2014 г. Divine Bulgaria. All rights reserved.
//

import UIKit

public class CustomPercentDrivenInteractiveTransition: UIPercentDrivenInteractiveTransition {
    public var isInteractive = false
}
