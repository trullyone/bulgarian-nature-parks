//
//  ViewController.swift
//  BulgarianNatureParks
//
//  Created by Nickolay Kunev on 19.09.14.
//  Copyright (c) 2014 г. Divine Bulgaria. All rights reserved.
//

import UIKit

class MainScreenViewController: CustomTransitioningViewController, UICollectionViewDataSource, UICollectionViewDelegate
     {

    var items = [1, 2, 3, 4, 1]
    
    override func viewDidLoad() {
        super.viewDidLoad()
     }

    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let result = collectionView.dequeueReusableCellWithReuseIdentifier("cell1", forIndexPath: indexPath) as UICollectionViewCell
        return result;
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.items.count
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        self.performSegueWithIdentifier("msdetails", sender: self);
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!) {
        super.prepareForSegue(segue, sender: sender)
        self.animatedTransitioningController?.setViewControlerTransition(AnimationHelpers.createFlipPage3dAnimation(self.animatedTransitioningController!.duration))
    }
}

