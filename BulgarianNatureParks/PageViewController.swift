//
//  PageViewController.swift
//  BulgarianNatureParks
//
//  Created by Nickolay Kunev on 02.10.14.
//  Copyright (c) 2014 г. Divine Bulgaria. All rights reserved.
//

import UIKit

class PageViewController: CustomTransitioningViewController {

    var startScale : CGFloat = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    @IBAction func handlePinchGesture(pinchGestureRecognizer: UIPinchGestureRecognizer) {
       self.startScale = GestureRecognizersHelpers.handlePopPinchGesture(pinchGestureRecognizer, startScale: self.startScale, interactiveTransitioningController: self.interactiveTransitioningController!, navigationController: self.navigationController)
    }
    
    @IBAction func handlePanGesture(panGestureRecognizer: UIPanGestureRecognizer) {
        GestureRecognizersHelpers.handlePopPanGesture(panGestureRecognizer, interactiveTransitioningController:
            self.interactiveTransitioningController!, navigationController: self.navigationController)
    }
}
