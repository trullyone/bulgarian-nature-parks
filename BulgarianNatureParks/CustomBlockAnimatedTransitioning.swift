//
//  CustomBlockAnimatedTransitioning.swift
//  BulgarianNatureParks
//
//  Created by Nickolay Kunev on 02.10.14.
//  Copyright (c) 2014 г. Divine Bulgaria. All rights reserved.
//

import UIKit

public class CustomBlockAnimatedTransitioning: NSObject, UIViewControllerAnimatedTransitioning {
    
    public var duration : NSTimeInterval = 0.5
    private var transitionAnimationBlock : ((transitionContext: UIViewControllerContextTransitioning,
        viewControllerFrom : UIViewController?, viewControllerTo : UIViewController?) -> Void)? = nil
    
    public func setViewControlerTransition(
            animationBlock: ((transitionContext: UIViewControllerContextTransitioning,
                fromViewController : UIViewController?, toViewController : UIViewController?) -> Void)?,
            withDuration: NSTimeInterval = 0.5) {
        
        self.transitionAnimationBlock = animationBlock
        self.duration = withDuration
    }
    
    public func transitionDuration(transitionContext: UIViewControllerContextTransitioning) -> NSTimeInterval
    {
        return self.duration
    }
    
    public func animateTransition(transitionContext: UIViewControllerContextTransitioning)
    {
        let fromVc = transitionContext.viewControllerForKey(UITransitionContextFromViewControllerKey)
        
        let fromView = fromVc?.view
        let toVc = transitionContext.viewControllerForKey(UITransitionContextToViewControllerKey)
        let toView = toVc?.view
        
        let containerView = transitionContext.containerView()
        containerView.addSubview(toView!)
        
        if self.transitionAnimationBlock != nil {
           self.transitionAnimationBlock!(transitionContext: transitionContext, viewControllerFrom: fromVc, viewControllerTo: toVc)
        }else{
            transitionContext.completeTransition(transitionContext.transitionWasCancelled() == false)
        }
    }
}
