//
//  AnimationHelpers.swift
//  BulgarianNatureParks
//
//  Created by Nickolay Kunev on 03.10.14.
//  Copyright (c) 2014 г. Divine Bulgaria. All rights reserved.
//

import UIKit

public class AnimationHelpers
{
    class public func createFlipPage3dAnimation(duration : NSTimeInterval) -> (transitionContext: UIViewControllerContextTransitioning,
        viewControllerFrom : UIViewController?, viewControllerTo : UIViewController?) -> Void
    {
        let result = {(ctx: UIViewControllerContextTransitioning,
            vcFrom : UIViewController?, vcTo : UIViewController?) -> Void in
            vcTo!.view.alpha = 0
            UIView.animateKeyframesWithDuration(duration, delay: 0, options: nil, animations: { () -> Void in
                UIView.addKeyframeWithRelativeStartTime(0, relativeDuration: duration, animations:
                    { () -> Void in
                        vcTo!.view.alpha = 1
                    })
                }, completion: { (completed : Bool) -> Void in
                    ctx.completeTransition(ctx.transitionWasCancelled() == false)
            })
        }
        
        return result
    }
}