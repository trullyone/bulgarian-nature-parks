//
//  GestureRecognizersHelpers.swift
//  BulgarianNatureParks
//
//  Created by Nickolay Kunev on 03.10.14.
//  Copyright (c) 2014 г. Divine Bulgaria. All rights reserved.
//

import UIKit

public class GestureRecognizersHelpers
{
    class func handlePopPinchGesture(pinchGestureRecognizer: UIPinchGestureRecognizer, startScale: CGFloat,
        interactiveTransitioningController : CustomPercentDrivenInteractiveTransition,
        navigationController : UINavigationController?) -> CGFloat
    {
        let scale = pinchGestureRecognizer.scale
        switch (pinchGestureRecognizer.state){
            case UIGestureRecognizerState.Began:
                interactiveTransitioningController.isInteractive = true
                navigationController?.popViewControllerAnimated(true)
                return scale
                //break
            case UIGestureRecognizerState.Changed:
                let percent = fmax(fmin(1.0 - scale / startScale, 1.00), 0.00)
                interactiveTransitioningController.updateInteractiveTransition(percent)
                break
            case UIGestureRecognizerState.Ended:
                interactiveTransitioningController.isInteractive = false
                let percent = fmax(fmin(1.0 - scale / startScale, 1.00), 0.00)
                if percent < 0.5 {
                    interactiveTransitioningController.cancelInteractiveTransition()
                }else{
                    interactiveTransitioningController.finishInteractiveTransition()
                }
                break
            case UIGestureRecognizerState.Cancelled:
                interactiveTransitioningController.isInteractive = false
                interactiveTransitioningController.cancelInteractiveTransition()
                break
            default:
                break
        }
        return startScale
    }
    
    class func handlePopPanGesture(panGestureRecognizer: UIPanGestureRecognizer,
        interactiveTransitioningController : CustomPercentDrivenInteractiveTransition,
        navigationController : UINavigationController?)
    {
        let view = panGestureRecognizer.view!
        let location = panGestureRecognizer.locationInView(view)
        let translation = panGestureRecognizer.translationInView(view)
        
        switch (panGestureRecognizer.state){
        case UIGestureRecognizerState.Began:
            if (location.x < CGRectGetMidX(view.bounds)){
                interactiveTransitioningController.isInteractive = true
                navigationController?.popViewControllerAnimated(true)
            }
            break
        case UIGestureRecognizerState.Changed:
            if interactiveTransitioningController.isInteractive == true{
                var d = (translation.x / CGRectGetWidth(view.bounds))
                interactiveTransitioningController.updateInteractiveTransition(d)
            }
            break
        case UIGestureRecognizerState.Ended:
            if interactiveTransitioningController.isInteractive == true{
                var d = (translation.x / CGRectGetWidth(view.bounds))
                if (panGestureRecognizer.velocityInView(view).x > 0 && d > 0.5) {
                    interactiveTransitioningController.finishInteractiveTransition()
                } else {
                    interactiveTransitioningController.cancelInteractiveTransition()
                }
                interactiveTransitioningController.isInteractive = false
            }
            break
        case UIGestureRecognizerState.Cancelled:
            if interactiveTransitioningController.isInteractive == true{
                interactiveTransitioningController.isInteractive = false
                interactiveTransitioningController.cancelInteractiveTransition()
            }
            break
        default:
            break
        }
    }
}